import java.math.BigInteger;

public class User {

    public static BigInteger g;
    public static BigInteger p;
    private BigInteger personalSecretKey;
    private BigInteger receivedSharedKey;
    private BigInteger finalSecretKey;

    public void generateSecretKey() {
        personalSecretKey = BigInteger.valueOf(Utils.generate(p.intValue()));
    }

    public String receiveSharedKey(String sharedKey, boolean encrypt) {
        if (encrypt) {
            this.receivedSharedKey = new BigInteger(Crypt.decrypt(sharedKey, 20));
        } else {
            this.receivedSharedKey = new BigInteger(sharedKey);
        }
        return receivedSharedKey.toString();
    }

    public String sendSharedKey(User dist, boolean encrypt) {
        BigInteger key = g.modPow(personalSecretKey, p);

        String keyString = key.toString();
        if (encrypt) {
            keyString = Crypt.encrypt(keyString, 20);
        }
        dist.receiveSharedKey(keyString, encrypt);
        return keyString;
    }

    public void calculateFinalKey() {
        finalSecretKey = receivedSharedKey.modPow(personalSecretKey, p);
    }

    public BigInteger getFinalSecretKey() {
        return finalSecretKey;
    }

    public BigInteger getPersonalSecretKey() {
        return personalSecretKey;
    }

    public BigInteger getReceivedSharedKey() {
        return receivedSharedKey;
    }
}
