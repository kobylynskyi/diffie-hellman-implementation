import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;

public class DiffieHellmanView extends JFrame {
    private JPanel mainPanel;
    private JButton sendKeysButton;
    private JTextArea aliceTextArea;
    private JTextArea bobTextArea;
    private JTextField alicePersonalSecretTextField;
    private JTextField bobPersonalSecretTextField;
    private JButton generateAllButton;
    private JTextField pTextField;
    private JTextField gTextField;
    private JButton calculateSharedSecretButton;
    private JButton clearAllButton;
    private JCheckBox encryptCheckBox;


    private User alice;
    private User bob;

    public DiffieHellmanView() {
        initComponents();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();

        alice = new User();
        bob = new User();
    }

    private void initComponents() {
        setResizable(false);
        setContentPane(mainPanel);
        setLocation(150, 150);

        generateAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                User.p = BigInteger.valueOf(Utils.getLargestPrime(1000));
                pTextField.setText(User.p.toString());

                User.g = BigInteger.valueOf(Utils.generate(User.p.intValue()));
                gTextField.setText(User.g.toString());

                alice.generateSecretKey();
                alicePersonalSecretTextField.setText(alice.getPersonalSecretKey().toString());
                aliceTextArea.setText("Personal secret key : " + alice.getPersonalSecretKey().toString() + "\n\n");

                bob.generateSecretKey();
                bobPersonalSecretTextField.setText(bob.getPersonalSecretKey().toString());
                bobTextArea.setText("Personal secret key : " + bob.getPersonalSecretKey().toString() + "\n\n");

                sendKeysButton.setEnabled(true);
                clearAllButton.setEnabled(true);
                encryptCheckBox.setSelected(true);
                encryptCheckBox.setEnabled(true);
            }
        });

        sendKeysButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String sentKeyToBob = alice.sendSharedKey(bob, encryptCheckBox.isSelected());
                String sentKeyToAlice = bob.sendSharedKey(alice, encryptCheckBox.isSelected());
                aliceTextArea.append(String.format("Calculating shared key:\n(%s^%s) mod %s = %s -> %s\n\n", User.g, alice.getPersonalSecretKey(), User.p, bob.getReceivedSharedKey(), sentKeyToAlice));
                bobTextArea.append(String.format("Calculating shared key:\n(%s^%s) mod %s = %s -> %s\n\n", User.g, bob.getPersonalSecretKey(), User.p, alice.getReceivedSharedKey(), sentKeyToBob));
                aliceTextArea.append(String.format("Received key from Bob: %s\n\n", sentKeyToAlice));
                bobTextArea.append(String.format("Received key from Alice: %s\n\n", sentKeyToBob));

                calculateSharedSecretButton.setEnabled(true);
                generateAllButton.setEnabled(false);
                sendKeysButton.setEnabled(false);
                encryptCheckBox.setEnabled(false);
            }
        });

        calculateSharedSecretButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                alice.calculateFinalKey();
                bob.calculateFinalKey();

                aliceTextArea.append(String.format("Calculating final secret key:\n(%s^%s) mod %s = %s\n", alice.getReceivedSharedKey(), alice.getPersonalSecretKey(), User.p, alice.getFinalSecretKey()));
                bobTextArea.append(String.format("Calculating final secret key:\n(%s^%s) mod %s = %s\n", bob.getReceivedSharedKey(), bob.getPersonalSecretKey(), User.p, bob.getFinalSecretKey()));
                generateAllButton.setEnabled(false);
                sendKeysButton.setEnabled(false);
                calculateSharedSecretButton.setEnabled(false);
                encryptCheckBox.setEnabled(false);
            }
        });

        clearAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                alicePersonalSecretTextField.setText("");
                bobPersonalSecretTextField.setText("");
                aliceTextArea.setText("");
                bobTextArea.setText("");
                pTextField.setText("");
                gTextField.setText("");
                alice = new User();
                bob = new User();
                generateAllButton.setEnabled(true);
                sendKeysButton.setEnabled(false);
                clearAllButton.setEnabled(false);
                calculateSharedSecretButton.setEnabled(false);
                encryptCheckBox.setEnabled(true);
            }
        });
    }


    public static void main(String[] args) {
        new DiffieHellmanView().setVisible(true);
    }
}
