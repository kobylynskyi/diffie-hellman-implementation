import junit.framework.Assert;
import org.junit.Test;

import java.math.BigInteger;

public class TestDiffieHellman {

    @Test
    public void test1() throws Exception {
        User.p = BigInteger.valueOf(Utils.getLargestPrime(1000));
        User.g = BigInteger.valueOf(Utils.generate(User.p.intValue()));

        User alice = new User();
        User bob = new User();

        alice.generateSecretKey();
        bob.generateSecretKey();

        alice.sendSharedKey(bob, true);
        bob.sendSharedKey(alice, true);

        alice.calculateFinalKey();
        bob.calculateFinalKey();

        Assert.assertEquals(alice.getFinalSecretKey(), bob.getFinalSecretKey());
    }

}
